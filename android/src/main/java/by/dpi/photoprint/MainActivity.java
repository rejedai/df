package by.dpi.photoprint;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.view.*;
import android.widget.*;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import by.dpi.photoprint.model.RequestCodes;
import by.dpi.photoprint.model.Order;

import java.util.*;

public class MainActivity extends AppCompatActivity {

    private boolean isExpandPrintLayout;
    private boolean isShowDialog;
    private boolean carouselItemClick = true;

    private int carouselPosition;

    private List<Order> orders;

    private SharedPreferencesService sharedPreferencesService;

    private TextView textViewTel;
    private TextView textViewResult;
    private TextView carouselTextView;
    private TextView lastResultTextView;
    private TextView deliveryTextView;

    private Button printButton;
    private Button cancelPrintButton;

    private RadioGroup radioGroup;
    private ArrayList<RadioButton> radioButtons;

    private Timer carouselTimer;

    private Toolbar toolbar;

    private ConstraintLayout printLayout;
    private ConstraintLayout dialogLayout;
    private ConstraintLayout blackoutLayout;
    private ConstraintLayout.LayoutParams printButtonLayoutParams;

    private OrderAdapterPrint orderAdapterPrint;

    private RecyclerView carouselRecyclerView;
    private RecyclerView orderRecyclerView;

    private int position;

    private DialogLayout dialog;

    private int dp(int pixels) {
        return Math.round(pixels * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferencesService = new SharedPreferencesService(getApplicationContext());

        textViewTel = findViewById(R.id.userTel);
        textViewResult = findViewById(R.id.textViewResult);
        carouselTextView = findViewById(R.id.carouselTextView);
        lastResultTextView = findViewById(R.id.lastResultTextView);
        deliveryTextView = findViewById(R.id.deliveryTextView);

        printButton = findViewById(R.id.printButton);
        cancelPrintButton = findViewById(R.id.cancelPrintButton);

        radioGroup = findViewById(R.id.carouselRadioGroup);

        radioButtons = new ArrayList<>();
        radioButtons.add(findViewById(R.id.carouselRadioButtonOne));
        radioButtons.add(findViewById(R.id.carouselRadioButtonTwo));
        radioButtons.add(findViewById(R.id.carouselRadioButtonThree));

        carouselTimer = new Timer();

        toolbar = findViewById(R.id.toolbar);

        printLayout = findViewById(R.id.printLayout);
        dialogLayout = findViewById(R.id.dialogLayout);
        blackoutLayout = findViewById(R.id.layout_blackout);

        carouselRecyclerView = findViewById(R.id.carouselRecyclerView);
        orderRecyclerView = findViewById(R.id.orderRecyclerView);

        orderAdapterPrint = new OrderAdapterPrint(this);

        dialog = new DialogLayout(this);

        setParameters();
        setListeners();
    }

    private void setParameters() {

        if (MainViewModel.getInstance() == null)
            new ViewModelProvider(this).get(MainViewModel.class);
        orders = MainViewModel.getInstance().getOrders();
        if (orders == null) {
            orders = new ArrayList<>();
            addNewOrder();
        }
        orderAdapterPrint.onAddNotify(orders);

        carouselTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                carouselPosition++;
                if (carouselPosition > 2)
                    carouselPosition = 0;
                runOnUiThread(() -> selectRadioButton(carouselPosition));
            }
        }, 0, 3000);

        setSupportActionBar(toolbar);

        LinearLayoutManager carouselLayoutManager = new LinearLayoutManager(getBaseContext());
        carouselLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        CarouselRecyclerAdapter carouselRecyclerAdapter = new CarouselRecyclerAdapter(this);
        carouselRecyclerView.setLayoutManager(carouselLayoutManager);
        carouselRecyclerView.setAdapter(carouselRecyclerAdapter);

        LinearLayoutManager ordersLayoutManager = new LinearLayoutManager(getBaseContext());
        orderRecyclerView.setLayoutManager(ordersLayoutManager);
        orderRecyclerView.setAdapter(orderAdapterPrint);

        printLayout.setElevation(dp(20));
        dialogLayout.setElevation(dp(40));
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setListeners() {
        printButton.setOnClickListener(v -> {
            if (!isExpandPrintLayout) expandPrintLayout();
            else {
                Intent intent = new Intent(getBaseContext(), ProgressActivity.class);
                startActivityForResult(intent, RequestCodes.ACTIVITY_PROGRESS.getRequestCode());
            }
        });

        cancelPrintButton.setOnClickListener(v -> expandPrintLayout());

        blackoutLayout.setOnClickListener(v -> {
            if (isExpandPrintLayout) expandPrintLayout();
            else showDialogLayout();
        });

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioButton = findViewById(checkedId);
            carouselRecyclerView.scrollToPosition(radioButtons.lastIndexOf(radioButton));
        });

        carouselRecyclerView.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case (MotionEvent.ACTION_DOWN):
                case (MotionEvent.ACTION_MOVE):
                    carouselItemClick = true;
                    break;
                case (MotionEvent.ACTION_UP):
                    carouselItemClick = false;
                    break;
            }
            return false;
        });

    }

    public void onCarouselItemClick(int position) {
        if (carouselItemClick)
            selectRadioButton(position);
    }

    public void selectRadioButton(int position) {
        radioGroup.check(radioButtons.get(position).getId());
    }

    private void addNewOrder() {
        Order order = new Order();
        order.num = orders.size();
        orders.add(order);
        orderAdapterPrint.onAddNotify(orders);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (sharedPreferencesService.isPhoneNumberNull())
            startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, WebActivity.class);
        int id = item.getItemId();
        switch (id) {
            case (R.id.ver_app):
                PackageInfo packageInfo = null;
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                assert packageInfo != null;
                String ver = packageInfo.versionName;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Версия программы:");
                builder.setMessage(ver);
                builder.setPositiveButton("Ок", (dialog, which) -> {
                });
                builder.show();
                break;
          /* TODO
             case (R.id.exit):
                finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());
                break;*/
            case (R.id.price):
                intent.putExtra("content", "price.html");
                startActivityForResult(intent, RequestCodes.PRICE_SHOW.getRequestCode());
                break;
            case (R.id.delivery):
                intent.putExtra("content", "delivery.html");
                startActivityForResult(intent, RequestCodes.PRICE_SHOW.getRequestCode());
                break;
            case (R.id.share_menuitem):
                startShareActivity();
                break;
            case (R.id.rate_menuitem):
                startRateActivity();
                break;
            case (R.id.logout_menuitem):
                sharedPreferencesService.removePhoneNumber();
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        textViewTel.setText(sharedPreferencesService.getPhoneNumber());
        printButton.setVisibility(View.GONE);

        if (orders.size() > 1) {
            printButton.setVisibility(View.VISIBLE);
            setCarouselVisibility(View.GONE);
        } else setCarouselVisibility(View.VISIBLE);

        if (orders == null || orders.size() < 2)
            textViewResult.setText("Итого: 0.00");
        else {
            textViewResult.setText("Итого: " + String.format("%.2f", Math.max(ordersGetResult(), 3f)));
            lastResultTextView.setText("ИТОГО заказ: " + String.format("%.2f", Math.max(ordersGetResult(), 3f)) + " руб");
        }
        MainViewModel.getInstance().setOrders(orders);

        isExpandPrintLayout = false;
        //isShowRateLayout = false;

        dialogLayout.setAlpha(0f);
        dialogLayout.setVisibility(View.INVISIBLE);

        cancelPrintButton.setVisibility(View.INVISIBLE);

        textViewTel.setAlpha(1f);
        textViewResult.setAlpha(1f);
        deliveryTextView.setAlpha(0f);
        lastResultTextView.setAlpha(0f);

        printButtonLayoutParams = (ConstraintLayout.LayoutParams) printButton.getLayoutParams();
        printButtonLayoutParams.topMargin = printButtonLayoutParams.bottomMargin;
        printButton.setLayoutParams(printButtonLayoutParams);

        blackoutLayout.setVisibility(View.INVISIBLE);
    }

    private void setCarouselVisibility(int visibility) {
        carouselRecyclerView.setVisibility(visibility);
        radioGroup.setVisibility(visibility);
        carouselTextView.setVisibility(visibility);
    }

    private float ordersGetResult() {
        float result = 0;
        for (Order order : orders)
            result = result + order.getPrice();
        return result;
    }

    public void onItemClickChanger() {
        onResume();
    }

    public void onItemClickDelete(int position) {
        orders.remove(position);
        orderAdapterPrint.onAddNotify(orders);
        onResume();
    }

    public void onItemClickImage(int position) {
        this.position = position;
        Intent i = new Intent();
        i.setType("image/*");
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Выберите файл"), RequestCodes.PICTURE_REQUEST_CODE.getRequestCode());
        onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestCodes.PICTURE_REQUEST_CODE.getRequestCode() && resultCode == RESULT_OK && null != data) {
            if (data.getClipData() == null) {
                Uri selectedImage = data.getData();
                //File file = new File(selectedImage.getPath());
                Order order = new Order(selectedImage);
                order.size = orders.get(position).size;
                order.quantity = orders.get(position).quantity;
                order.quality = orders.get(position).quality;
                orders.add(position, order);
            } else {
                int size = orders.get(position).size;
                int quantity = orders.get(position).quantity;
                int quality = orders.get(position).quality;
                for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                    Order order = new Order(data.getClipData().getItemAt(i).getUri());
                    order.quantity = quantity;
                    order.size = size;
                    order.quality = quality;
                    orders.add(position, order);
                }
            }
            orderAdapterPrint.onAddNotify(orders);
            onResume();
        }
        if (requestCode == RequestCodes.CAROUSEL_SHOW.getRequestCode() && resultCode == RESULT_OK) {
            Intent i = new Intent();
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            i.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(i, "Выберите файл"), RequestCodes.PICTURE_REQUEST_CODE.getRequestCode());
            onResume();
        }
        if (requestCode == RequestCodes.ACTIVITY_PROGRESS.getRequestCode())
            if (resultCode == RESULT_OK) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Загрузка завершена.");
                builder.setMessage("Загружено: " + (orders.size() - 1) + " файлов.");
                builder.setPositiveButton("Ок", (dialog, which) -> {
                    orders.clear();
                    addNewOrder();

                    sharedPreferencesService.incrementOrderCount();
                    long date = Calendar.getInstance().getTime().getTime();
                    if (sharedPreferencesService.getOrderCount() >= 1 && sharedPreferencesService.getLastRateShow() < date - 7776000000L /* 3 mon */) {
                        showRateDialog();
                        sharedPreferencesService.setLastRateShow(date);
                    } else if (sharedPreferencesService.getOrderCount() >= 2 && sharedPreferencesService.getLastShareShow() < date - 15638400000L /* 6 mon */) {
                        showShareDialog();
                        sharedPreferencesService.setLastShareShow(date);
                    } else onResume();
                });
                builder.show();
            }
        if (requestCode == RequestCodes.SHARE_ACTIVITY.getRequestCode() && isShowDialog) {
            showShareDialog();
        }
        if (requestCode == RequestCodes.MARKET_ACTIVITY.getRequestCode() && isShowDialog) {
            showDialogLayout();
        }
    }

    private void expandPrintLayout() {
        isExpandPrintLayout = !isExpandPrintLayout;

        blackoutLayout.setElevation(dp(10));

        float startAlphaAnimator = isExpandPrintLayout ? 0f : 1f;
        float endAlphaAnimator = isExpandPrintLayout ? 1f : 0f;

        final ValueAnimator alphaAnimator = ValueAnimator.ofFloat(startAlphaAnimator, endAlphaAnimator);
        alphaAnimator.setDuration(200);
        alphaAnimator.addUpdateListener(animation -> {
            blackoutLayout.setAlpha((Float) animation.getAnimatedValue() / 3f);
            cancelPrintButton.setAlpha((Float) animation.getAnimatedValue());
            textViewTel.setAlpha(1f - (Float) animation.getAnimatedValue());
            textViewResult.setAlpha(1f - (Float) animation.getAnimatedValue());
            deliveryTextView.setAlpha((Float) animation.getAnimatedValue());
            lastResultTextView.setAlpha((Float) animation.getAnimatedValue());
            if (animation.getAnimatedValue().equals(0f)) {
                int visibility = isExpandPrintLayout ? View.VISIBLE : View.INVISIBLE;
                blackoutLayout.setVisibility(visibility);
                cancelPrintButton.setVisibility(visibility);
            }
        });
        alphaAnimator.start();

        ConstraintLayout.LayoutParams lastResultTextViewLayoutParams = ((ConstraintLayout.LayoutParams) lastResultTextView.getLayoutParams());
        ConstraintLayout.LayoutParams deliveryTextViewLayoutParams = ((ConstraintLayout.LayoutParams) deliveryTextView.getLayoutParams());
        printButtonLayoutParams = (ConstraintLayout.LayoutParams) printButton.getLayoutParams();

        int closedValue = printButtonLayoutParams.bottomMargin;
        int expandedValue = lastResultTextViewLayoutParams.topMargin +
                lastResultTextViewLayoutParams.getConstraintWidget().getHeight() +
                lastResultTextViewLayoutParams.bottomMargin +
                deliveryTextViewLayoutParams.getConstraintWidget().getHeight() +
                deliveryTextViewLayoutParams.bottomMargin +
                dp(10) +
                closedValue;

        int startValueAnimator = isExpandPrintLayout ? closedValue : expandedValue;
        int endValueAnimator = isExpandPrintLayout ? expandedValue : closedValue;

        final ValueAnimator valueAnimator = ValueAnimator.ofInt(startValueAnimator, endValueAnimator);
        valueAnimator.setDuration(200);
        valueAnimator.addUpdateListener(animation -> {
            printButtonLayoutParams.topMargin = (Integer) valueAnimator.getAnimatedValue();
            printButton.setLayoutParams(printButtonLayoutParams);
        });
        valueAnimator.start();
    }

    public void showDialogLayout() {
        isShowDialog = !isShowDialog;

        blackoutLayout.setElevation(dp(30));

        float startAlphaAnimator = isShowDialog ? 0f : 1f;
        float endAlphaAnimator = isShowDialog ? 1f : 0f;

        final ValueAnimator alphaAnimator = ValueAnimator.ofFloat(startAlphaAnimator, endAlphaAnimator);
        alphaAnimator.setDuration(200);
        alphaAnimator.addUpdateListener(animation -> {
            blackoutLayout.setAlpha((Float) animation.getAnimatedValue() / 2.5f);
            dialogLayout.setAlpha((Float) animation.getAnimatedValue());
            if (animation.getAnimatedValue().equals(0f)) {
                int visibility = isShowDialog ? View.VISIBLE : View.INVISIBLE;
                blackoutLayout.setVisibility(visibility);
                dialogLayout.setVisibility(visibility);
            }
        });
        alphaAnimator.start();
    }

    public void showShareDialog() {
        dialog.setDialogTitle(R.string.share_dialog_title);

        dialog.setLeftButton(R.string.decline_button, v -> {
            showDialogLayout();
            onResume();
        });

        dialog.setRightButton(R.string.confirm_button, v -> {
            startShareActivity();

            onResume();
        });

        showDialogLayout();
    }

    private void startShareActivity() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Я печатаю фото через приложение \"Улыбочку!\". И тебе рекомендую!\n\nСкачать: https://dpi.by/foto";
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivityForResult(Intent.createChooser(sharingIntent, "Поделиться через"), RequestCodes.ACTIVITY_PROGRESS.getRequestCode());
    }

    public void showRateDialog() {
        dialog.setDialogTitle(R.string.rate_dialog_title);

        dialog.setLeftButton(R.string.decline_button, v -> {
            showDialogLayout();
            onResume();
        });

        dialog.setRightButton(R.string.confirm_button, v -> {
            startRateActivity();

            onResume();
        });

        showDialogLayout();
    }

    private void startRateActivity() {
        final String appPackageName = getPackageName();
        try {
            startActivityForResult(
                    new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)),
                    RequestCodes.MARKET_ACTIVITY.getRequestCode());
        } catch (android.content.ActivityNotFoundException e) {
            startActivityForResult(
                    new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)),
                    RequestCodes.MARKET_ACTIVITY.getRequestCode());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        carouselTimer.cancel();
    }
}