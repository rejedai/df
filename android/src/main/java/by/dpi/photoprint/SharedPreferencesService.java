package by.dpi.photoprint;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesService {
    final private SharedPreferences sharedPreferences;

    SharedPreferencesService(Context context) {
        sharedPreferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE);
    }

    public void setPhoneNumber(String phoneNumber) {
        sharedPreferences.edit()
                .putString("phone", phoneNumber)
                .apply();
    }

    public String getPhoneNumber() {
        return sharedPreferences.getString("phone", "");
    }

    public boolean isPhoneNumberNull() {
        return getPhoneNumber().isEmpty();
    }

    public void removePhoneNumber() {
        if (!isPhoneNumberNull()) {
            sharedPreferences.edit()
                    .putString("oldPhone", getPhoneNumber())
                    .remove("phone")
                    .apply();
        }
    }

    public String getOldPhoneNumber() {
        return sharedPreferences.getString("oldPhone", "");
    }

    public void incrementOrderCount() {
        String key = "order_count";
        sharedPreferences.edit()
                .putLong(key,
                        sharedPreferences.getLong(key, 0L) + 1L)
                .apply();
    }

    public long getOrderCount() {
        return sharedPreferences.getLong("order_count", 0L);
    }

    public void setLastShareShow(long date) {
        sharedPreferences.edit()
                .putLong("last_share_show", date)
                .apply();
    }

    public long getLastShareShow() {
        return sharedPreferences.getLong("last_share_show", 0L);
    }

    public void setLastRateShow(long date) {
        sharedPreferences.edit()
                .putLong("last_rate_show", date)
                .apply();
    }

    public long getLastRateShow() {
        return sharedPreferences.getLong("last_rate_show", 0L);
    }


}
