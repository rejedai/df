package by.dpi.photoprint;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class WebActivity extends AppCompatActivity {

    private android.webkit.WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        webView = findViewById(R.id.webWiew);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("UTF-8");
        webView.loadUrl("file:///android_res/raw/" + getIntent().getStringExtra("content"));

    }
}