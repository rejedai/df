package by.dpi.photoprint;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import by.dpi.photoprint.model.Order;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OrderAdapterPrint extends RecyclerView.Adapter<OrderAdapterPrint.ViewHolderPrint> {

    public static Uri defaultUri;
    private List<Order> orders;
    private final LayoutInflater layoutInflater;
    private final MainActivity mainActivity;

    public OrderAdapterPrint(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        layoutInflater = LayoutInflater.from(mainActivity);
        defaultUri = Uri.parse("android.resource://" + mainActivity.getPackageName() + "/drawable/defaulturi");
    }

    @NonNull
    @Override
    public ViewHolderPrint onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_order, parent, false);
        return new ViewHolderPrint(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderPrint holder, final int positionAdapter) {
        Uri uri = orders.get(positionAdapter).uri;

        int visibleType;
        if (uri.equals(defaultUri))
            visibleType = View.GONE;
        else
            visibleType = View.VISIBLE;

        holder.orderImageView.setVisibility(visibleType);
        holder.orderPriceTextView.setVisibility(visibleType);

        Picasso.get()
                .load(uri)
                .into(holder.orderImageView);
        holder.orderPriceTextView.setText(orders.get(positionAdapter).getSinglePrice());
        holder.orderImageView.setOnClickListener(v -> {
            mainActivity.onItemClickImage(positionAdapter);
            mainActivity.onItemClickChanger();
        });

        if (orders.get(positionAdapter).uri.equals(defaultUri)) {
            holder.deleteOrderButton.setVisibility(View.GONE);
            holder.addOrderButton.setOnClickListener(v -> {
                mainActivity.onItemClickImage(positionAdapter);
                mainActivity.onItemClickChanger();
            });
        } else {
            holder.deleteOrderButton.setVisibility(View.VISIBLE);
            holder.deleteOrderButton.setOnClickListener(v -> {
                mainActivity.onItemClickDelete(positionAdapter);
                holder.orderPriceTextView.setText(orders.get(positionAdapter).getSinglePrice());
            });
        }

        holder.orderSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                orders.get(positionAdapter).size = position;
                holder.orderPriceTextView.setText(orders.get(positionAdapter).getSinglePrice());
                mainActivity.onItemClickChanger();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        holder.orderSizeSpinner.setSelection(orders.get(positionAdapter).size);
        holder.orderQualitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                orders.get(positionAdapter).quality = position;
                holder.orderPriceTextView.setText(orders.get(positionAdapter).getSinglePrice());
                mainActivity.onItemClickChanger();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.orderQualitySpinner.setSelection(orders.get(positionAdapter).quality);
        holder.orderQuantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                orders.get(positionAdapter).quantity = (position + 1);
                holder.orderPriceTextView.setText(orders.get(positionAdapter).getSinglePrice());
                mainActivity.onItemClickChanger();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.orderQuantitySpinner.setSelection(orders.get(positionAdapter).quantity - 1);
        holder.orderQualitySpinner.setSelection(orders.get(positionAdapter).quality);
        holder.orderSizeSpinner.setSelection(orders.get(positionAdapter).size);
    }

    @Override
    public int getItemCount() {
        if (orders == null) return 0;
        return orders.size();
    }

    public void onAddNotify(List<Order> orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    public class ViewHolderPrint extends RecyclerView.ViewHolder {

        private final TextView orderPriceTextView;

        private final ImageView orderImageView;

        private final Button addOrderButton;
        private final Button deleteOrderButton;

        private final Spinner orderSizeSpinner;
        private final Spinner orderQualitySpinner;
        private final Spinner orderQuantitySpinner;

        public ViewHolderPrint(View view) {
            super(view);

            orderPriceTextView = view.findViewById(R.id.orderPriceTextView);

            orderImageView = view.findViewById(R.id.orderImageView);

            addOrderButton = view.findViewById(R.id.addOrderButton);
            deleteOrderButton = view.findViewById(R.id.deleteOrderButton);

            orderSizeSpinner = view.findViewById(R.id.orderSizeSpinner);
            ArrayAdapter<CharSequence> sizeAdapter = ArrayAdapter.createFromResource(mainActivity,
                    R.array.size_array, R.layout.spinersize);
            sizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            orderSizeSpinner.setAdapter(sizeAdapter);

            orderQualitySpinner = view.findViewById(R.id.orderQualitySpinner);
            ArrayAdapter<CharSequence> qualityAdapter = ArrayAdapter.createFromResource(mainActivity,
                    R.array.quality_array, R.layout.spinersize);
            qualityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            orderQualitySpinner.setAdapter(qualityAdapter);

            orderQuantitySpinner = view.findViewById(R.id.orderQuantitySpinner);
            ArrayAdapter<CharSequence> quantityAdapter = ArrayAdapter.createFromResource(mainActivity,
                    R.array.quantity_array, R.layout.spinersize);
            quantityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            orderQuantitySpinner.setAdapter(quantityAdapter);
        }
    }
}
