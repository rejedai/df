package by.dpi.photoprint.model;

public class User {

    String phoneNumber;
    String oldPhoneNumber;

    public User() { }

    public User(String phoneNumber, String oldPhoneNumber) {
        this.phoneNumber = phoneNumber;
        this.oldPhoneNumber = oldPhoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOldPhoneNumber() {
        return oldPhoneNumber;
    }

    public void setOldPhoneNumber(String oldPhoneNumber) {
        this.oldPhoneNumber = oldPhoneNumber;
    }
}
