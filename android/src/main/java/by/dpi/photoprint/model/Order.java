package by.dpi.photoprint.model;

import android.net.Uri;
import by.dpi.photoprint.OrderAdapterPrint;

import java.io.File;

public class Order {

    /** TODO
     * change public to private
     */
    public int size;
    public int quality;
    public int quantity;
    public  int num;

    public Uri uri;

    private void init() {
        size = 0;
        quality = 0;
        quantity = 1;
        num = 1;
    }

    public Order() {
       init();
       this.uri = OrderAdapterPrint.defaultUri;
    }

    public Order(Uri uri) {
      init();
      this.uri = uri;
        }

    public float getPrice() {
        float ret;
        ret = Size.size[quality][size].getPrice() * quantity;
        if (uri.equals(OrderAdapterPrint.defaultUri)) ret = 0;
        return ret;
    }

    public String getSinglePrice(){
        return Size.size[quality][size].getPrice() + "P";
    }

    /** TODO
     * Check how works and for what?
     *
     *
    public String toString() {

        return "Order{" +
                ", size='" + Size.size[quality][size].name + '\'' +
                ", quality='" + Quality.quality[quality] + '\'' +
                ", quantity='" + quantity + '\'' +
                ", num=" + num +
                ", uri=" + uri +
                '}';
    }
     */

    public String getFileDestination() {
        File file = new File(uri.getPath());
        String filename = file.getName();
        filename = filename.replace(":", "-");
         return Quality.quality[quality] + "-"
                   + Size.size[quality][size].name + " x "
                   + quantity + "шт"
                   + " file "
                   + filename;
    }
}
