package by.dpi.photoprint.model;

public enum RequestCodes {
    PICTURE_REQUEST_CODE(101), CAROUSEL_SHOW(102), ACTIVITY_PROGRESS(103), PRICE_SHOW(104), SHARE_ACTIVITY(105), MARKET_ACTIVITY(106);
    private final int requestCode;


    RequestCodes(int requestCode) {
        this.requestCode = requestCode;
    }

    public int getRequestCode() {
        return requestCode;
    }
}
