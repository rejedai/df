package by.dpi.photoprint.model;

public class Size {

    public static Item[][] size = {
            {new Item("10 x 15 (a6)", (float) 0.75),
                    new Item("13 x 18", (float) 2.00),
                    new Item("15 x 21 (a5)", (float) 2.00),
                    new Item("21 x 30 (a4)", (float) 3.50)},
            {new Item("10 x 15 (a6)", (float) 0.75),
                    new Item("13 x 18", (float) 2.00),
                    new Item("15 x 21 (a5)", (float) 2.00),
                    new Item("21 x 30 (a4)", (float) 3.50)},
            {new Item("10 x 15 (a6)", (float) 0.49),
                    new Item("13 x 18", (float) 1.00),
                    new Item("15 x 21 (a5)", (float) 1.00),
                    new Item("21 x 30 (a4)", (float) 2.50)},
            {new Item("10 x 15 (a6)", (float) 0.39),
                    new Item("13 x 18", (float) 0.75),
                    new Item("15 x 21 (a5)", (float) 0.75),
                    new Item("21 x 30 (a4)", (float) 1.50)},
            {new Item("10 x 15 (a6)", (float) 0.29),
                    new Item("13 x 18", (float) 0.75),
                    new Item("15 x 21 (a5)", (float) 0.65),
                    new Item("21 x 30 (a4)", (float) 1.25)}
    };
}