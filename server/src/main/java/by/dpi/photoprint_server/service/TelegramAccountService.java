package by.dpi.photoprint_server.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutionException;

@Service
public class TelegramAccountService {

    public static final String COL_NAME=System.getenv("DATABASE_COL_TELEGRAM_ACCOUNT");

    private final Firestore firestore;

    TelegramAccountService() {
        this.firestore = FirestoreClient.getFirestore();
    }

    public void addAccount(long accountId) throws InterruptedException, ExecutionException {

        ApiFuture<WriteResult> writeResultApiFuture = firestore.collection(COL_NAME)
                .document(String.valueOf(accountId)).set(new HashMap<>());
        writeResultApiFuture.get();
    }

    public List<String> getAccounts() throws  InterruptedException, ExecutionException {
        ArrayList<String> arrayList = new ArrayList<>();

        for (DocumentReference accountDocument: firestore.collection(COL_NAME).listDocuments()) {
            ApiFuture<DocumentSnapshot> future = accountDocument.get();
            DocumentSnapshot documentSnapshot = future.get();

            if(documentSnapshot.exists())
                arrayList.add(documentSnapshot.getId());
        }
        return arrayList;
    }

    public void deleteAccount(long accountId) throws InterruptedException, ExecutionException {
        ApiFuture<WriteResult> writeResult = firestore.collection(COL_NAME)
                .document(String.valueOf(accountId)).delete();
        writeResult.get();
    }

}
