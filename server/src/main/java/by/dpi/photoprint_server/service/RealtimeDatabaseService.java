package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.Order;
import by.dpi.photoprint_server.model.User;
import com.google.firebase.database.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutionException;

@Service
public class RealtimeDatabaseService {
    final TelegramService telegramService;
    final NewUserService newUserService;

    final FirebaseDatabase database;

    final DatabaseReference orderReference;
    final DatabaseReference userReference;

    RealtimeDatabaseService(TelegramService telegramService, NewUserService newUserService) {
        this.telegramService = telegramService;
        this.newUserService = newUserService;

        this.database = FirebaseDatabase.getInstance();

        this.orderReference = database.getReference(System.getenv("DATABASE_COL_ORDERS"));
        this.userReference = database.getReference(System.getenv("DATABASE_COL_USERS"));
    }

    @PostConstruct
    public void setListener() {
        orderReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                try {
                    onOrderChildAdded(dataSnapshot);
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }

                orderReference.child(dataSnapshot.getKey()).setValueAsync(null);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        userReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
                try {
                    onUserChildAdded(snapshot);
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
                userReference.child(snapshot.getKey()).setValueAsync(null);
            }

            @Override
            public void onChildChanged(DataSnapshot snapshot, String previousChildName) {

            }

            @Override
            public void onChildRemoved(DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot snapshot, String previousChildName) {

            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }

    void onOrderChildAdded(DataSnapshot dataSnapshot) throws ExecutionException, InterruptedException {
        Order order = dataSnapshot.getValue(Order.class);
        telegramService.sendNewOrderMessage(order);
    }

    void onUserChildAdded(DataSnapshot dataSnapshot) throws ExecutionException, InterruptedException {
        User user = dataSnapshot.getValue(User.class);

        if (!user.getOldPhoneNumber().isEmpty())
            telegramService.sendNumberChangeUserMessage(user);

        if (!newUserService.isUserExists(user.getUID())) {
            newUserService.saveUserDetails(dataSnapshot.getValue(User.class), user.getUID());
            telegramService.sendNewUserMessage(user);
        }
    }
}
