package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.Order;
import by.dpi.photoprint_server.model.User;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.concurrent.ExecutionException;

@Service
public class TelegramService extends TelegramLongPollingBot {

    @Override
    public String getBotUsername() {
        return System.getenv("TELEGRAM_BOT_USERNAME");
    }

    @Override
    public String getBotToken() {
        return System.getenv("TELEGRAM_TOKEN");
    }

    public void sendNewOrderMessage(Order order) throws ExecutionException, InterruptedException {
        sendNewEventMessage("Улыбочку! Поступил новый заказ от номера:\n<b>$PHONE</b>", order.getPhone());
    }

    public void sendNewUserMessage(User user) throws ExecutionException, InterruptedException {
        sendNewEventMessage("Зарегистрирован новый пользователь с номером:\n<b>$PHONE</b>", user.getPhoneNumber());
    }

    public void sendNumberChangeUserMessage(User user) throws ExecutionException, InterruptedException {
        sendNewEventMessage("Пользователь сменил номер:\nс <b>$PHONE1</b> на <b>$PHONE2</b>", user.getPhoneNumber(), user.getOldPhoneNumber());
    }

    private void sendNewEventMessage(String messageText, String... phones) throws InterruptedException, ExecutionException {
        StringBuilder[] phoneNumbers = new StringBuilder[phones.length];
        int i = 0;
        for (String phone : phones) {
            phoneNumbers[i] = new StringBuilder(phone);

            phoneNumbers[i].insert(11, " ")
                    .insert(9, " ")
                    .insert(6, " ")
                    .insert(4, " ");
            i++;
        }

        SendMessage sendMessage = new SendMessage();
        if (phoneNumbers.length == 1) sendMessage.setText(messageText.replace("$PHONE", phoneNumbers[0].toString()));
        else if (phoneNumbers.length == 2)
            sendMessage.setText(messageText.replace("$PHONE1", phoneNumbers[0].toString())
                    .replace("$PHONE2", phoneNumbers[1].toString()));
        else throw new InterruptedException();
        sendMessage.enableHtml(true);

        for (String accountId : new TelegramAccountService().getAccounts()) {
            sendMessage.setChatId(accountId);
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            SendMessage message = new SendMessage();
            message.setChatId(update.getMessage().getChatId().toString());
            message.setText("Неверный запрос");

            if (update.getMessage().isCommand()) {
                if (update.getMessage().getText().equals("/subscribe admin")) {
                    try {
                        new TelegramAccountService().addAccount(update.getMessage().getChatId());
                        message.setText("Вы подписаны на рассылку новых заказов");
                    } catch (InterruptedException | ExecutionException e) {
                        message.setText("Не удалось подписаться на рассылку");
                        e.printStackTrace();
                    }
                } else if (update.getMessage().getText().equals("/unsubscribe")) {
                    try {
                        new TelegramAccountService().deleteAccount(update.getMessage().getChatId());
                        message.setText("Вы отписались от рассылки");
                    } catch (InterruptedException | ExecutionException e) {
                        message.setText("Не удалось отписать от рассылки");
                        e.printStackTrace();
                    }
                }
            }

            try {
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }
}
