package by.dpi.photoprint_server.model;

public class User {

    String phoneNumber;
    String oldPhoneNumber;

    User() {
    }

    public User(String phoneNumber, String oldPhoneNumber) {
        this.phoneNumber = phoneNumber;
        this.oldPhoneNumber = oldPhoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOldPhoneNumber() {
        return oldPhoneNumber;
    }

    public void setOldPhoneNumber(String oldPhoneNumber) {
        this.oldPhoneNumber = oldPhoneNumber;
    }

    public String getUID() {
        return phoneNumber.replaceAll("[^0-9]", "");
    }
}
