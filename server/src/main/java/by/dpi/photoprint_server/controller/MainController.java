package by.dpi.photoprint_server.controller;

import by.dpi.photoprint_server.service.CounterService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Controller
public class MainController {

    final CounterService counterService;

    MainController(CounterService counterService) {
        this.counterService = counterService;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("telegram_active", "Active");
        model.addAttribute("firebase_active", "Active");
        model.addAttribute("server_version", getServerVersion());

        return "index";
    }

    private String getServerVersion() {
        String serverVer;
        try {
            Properties prop = new Properties();
            InputStream input = new FileInputStream("gradle.properties");
            prop.load(input);
            serverVer = prop.getProperty("version_tag");
        } catch (IOException ex) {
            serverVer = "Unknown";
            ex.printStackTrace();
        }
        return serverVer;
    }


    @GetMapping("/photo")
    public String photo() {
        return "photo";
    }

    @GetMapping("/_easter_egg")
    public String easterEgg(
            @CookieValue(value = "reloadCount", defaultValue = "0") long reloadCount,
            @RequestParam(name="name", required=false, defaultValue="World") String name,
            Model model,
            HttpServletResponse response) throws ExecutionException, InterruptedException {

        long serverCount = counterService.getCounterDetails();
        serverCount++;
        counterService.updateCounterDetails(serverCount);

        reloadCount++;
        Cookie cookie = new Cookie("reloadCount", String.valueOf(reloadCount));
        response.addCookie(cookie);

        model.addAttribute("name", name);
        model.addAttribute("server_count", String.valueOf(serverCount));
        model.addAttribute("local_count", String.valueOf(reloadCount));
        return "_easter_egg";
    }
}